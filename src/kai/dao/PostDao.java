package kai.dao;

import static kai.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import kai.beans.Post;
import kai.exception.NoRowsUpdatedRuntimeException;
import kai.exception.SQLRuntimeException;

public class PostDao {

    public void insert(Connection connection, Post post) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO posts ( ");
            sql.append("  user_id");
            sql.append(", title");
            sql.append(", text");
            sql.append(", category");
            sql.append(", is_deleted");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append("?"); // user_id
            sql.append(", ?"); // title
            sql.append(", ?"); // text
            sql.append(", ?"); // category
            sql.append(", ?"); // is_deleted
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, post.getUserId());
            ps.setString(2, post.getTitle());
            ps.setString(3, post.getText());
            ps.setString(4, post.getCategory());
            ps.setInt(5, post.getIsDeleted());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public void postDelete(Connection connection, Post post) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE posts SET  is_deleted = 0, updated_date = CURRENT_TIMESTAMP WHERE id = ?;");

			ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, post.getId());

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

}