package kai.dao;

import static kai.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import kai.beans.UserPost;
import kai.exception.SQLRuntimeException;

public class UserPostDao {

    public List<UserPost> getUserPosts(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("posts.id as id, ");
            sql.append("posts.text as text, ");
            sql.append("posts.user_id as user_id, ");
            sql.append("users.name as name, ");
            sql.append("posts.category as category, ");
            sql.append("posts.is_deleted as is_deleted, ");
            sql.append("posts.title as title, ");
            sql.append("posts.created_date as created_date ");
            sql.append("FROM posts ");
            sql.append("INNER JOIN users ");
            sql.append("ON posts.user_id = users.id ");
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserPost> ret = toUserPostList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserPost> toUserPostList(ResultSet rs)
            throws SQLException {

        List<UserPost> ret = new ArrayList<UserPost>();
        try {
            while (rs.next()) {
                String name = rs.getString("name");
                int id = rs.getInt("id");
                int userId = rs.getInt("user_id");
                String category = rs.getString("category");
                String text = rs.getString("text");
                int isDeleted = rs.getInt("is_deleted");
                String title = rs.getString("title");
                Timestamp createdDate = rs.getTimestamp("created_date");

                UserPost post = new UserPost();
                post.setName(name);
                post.setId(id);
                post.setUserId(userId);
                post.setCategory(category);
                post.setText(text);
                post.setIsDeleted(isDeleted);
                post.setTitle(title);
                post.setCreatedDate(createdDate);

                ret.add(post);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

    public List<UserPost> getUserPosts(Connection connection, Date dateStart, Date dateEnd, String category, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("posts.id as id, ");
            sql.append("posts.text as text, ");
            sql.append("posts.user_id as user_id, ");
            sql.append("users.name as name, ");
            sql.append("posts.category as category, ");
            sql.append("posts.is_deleted as is_deleted, ");
            sql.append("posts.title as title, ");
            sql.append("posts.created_date as created_date ");
            sql.append("FROM posts ");
            sql.append("INNER JOIN users ");
            sql.append("ON posts.user_id = users.id ");
            sql.append("WHERE posts.created_date BETWEEN ? AND ? ");
            if (category != null && !category.isEmpty()) {
            	sql.append("AND category LIKE ? ");
            }
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ps.setDate(1, dateStart);
            ps.setDate(2, dateEnd);
            if (category != null && !category.isEmpty()) {
            	ps.setString(3,"%" + category + "%");
            }


            ResultSet rs = ps.executeQuery();
            List<UserPost> ret = toUserPostList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

}