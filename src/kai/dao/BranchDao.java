package kai.dao;

import static kai.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import kai.beans.Branch;
import kai.exception.SQLRuntimeException;

public class BranchDao {

	/*
	//支店が増えたら使ってください。
    public void insert(Connection connection, Branch branch) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO branches ( ");
            sql.append("id");
            sql.append(", name");
            sql.append(") VALUES (");
            sql.append("?"); // id
            sql.append(", ?"); // name
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, branch.getId());
            ps.setString(2, branch.getName());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }*/

    public List<Branch> branchesList(Connection connection) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM branches;";

            ps = connection.prepareStatement(sql);

            ResultSet rs = ps.executeQuery();
            List<Branch> branchList = toBranchList(rs);
            return branchList;
    	} catch (SQLException e) {
    		throw new SQLRuntimeException(e);
    	} finally {
    		close(ps);
    	}
    }

    private List<Branch> toBranchList(ResultSet rs) throws SQLException {

        List<Branch> ret = new ArrayList<Branch>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");

                Branch branch = new Branch();
                branch.setId(id);
                branch.setName(name);

                ret.add(branch);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

    /*
    public Branch getBranch(Connection connection, int id) {

    	PreparedStatement ps = null;
    	try {
    		String sql = "SELECT * FROM branches WHERE id = ?";

    		ps = connection.prepareStatement(sql);
    		ps.setInt(1, id);

    		ResultSet rs = ps.executeQuery();
    		List<Branch> branchList = toBranchList(rs);
    		if (branchList.isEmpty() == true) {
    			return null;
    		} else if (2 <= branchList.size()) {
    			throw new IllegalStateException("2 <= branchList.size()");
    		} else {
    			return branchList.get(0);
    		}
    	} catch (SQLException e) {
    		throw new SQLRuntimeException(e);
    	} finally {
    		close(ps);
    	}
    }
    //支店名が変わったら使ってください。
    public void update(Connection connection, Branch branch) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE branches SET");
			sql.append("  id = ?");
			sql.append(", name = ?");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, branch.getId());
            ps.setString(2, branch.getName());

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}*/

}