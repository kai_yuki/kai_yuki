package kai.dao;

import static kai.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import kai.beans.Position;
import kai.exception.SQLRuntimeException;

public class PositionDao {

	/*
	//役職が増えたら使ってください。
    public void insert(Connection connection, Position position) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO positions ( ");
            sql.append("id");
            sql.append(", name");
            sql.append(") VALUES (");
            sql.append("?"); // id
            sql.append(", ?"); // name
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, position.getId());
            ps.setString(2, position.getName());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }*/

    public List<Position> positionsList(Connection connection) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM positions;";

            ps = connection.prepareStatement(sql);

            ResultSet rs = ps.executeQuery();
            List<Position> positionList = toPositionList(rs);
            return positionList;
    	} catch (SQLException e) {
    		throw new SQLRuntimeException(e);
    	} finally {
    		close(ps);
    	}
    }

    private List<Position> toPositionList(ResultSet rs) throws SQLException {

        List<Position> ret = new ArrayList<Position>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");

                Position position = new Position();
                position.setId(id);
                position.setName(name);

                ret.add(position);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

    /*
    public Position getPosition(Connection connection, int id) {

    	PreparedStatement ps = null;
    	try {
    		String sql = "SELECT * FROM positions WHERE id = ?";

    		ps = connection.prepareStatement(sql);
    		ps.setInt(1, id);

    		ResultSet rs = ps.executeQuery();
    		List<Position> positionList = toPositionList(rs);
    		if (positionList.isEmpty() == true) {
    			return null;
    		} else if (2 <= positionList.size()) {
    			throw new IllegalStateException("2 <= positionList.size()");
    		} else {
    			return positionList.get(0);
    		}
    	} catch (SQLException e) {
    		throw new SQLRuntimeException(e);
    	} finally {
    		close(ps);
    	}
    }
    //役職名が変わったら使ってください。
    public void update(Connection connection, Position position) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE positions SET");
			sql.append("  id = ?");
			sql.append(", name = ?");
			sql.append(" WHERE");
			sql.append(" id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, position.getId());
            ps.setString(2, position.getName());

			int count = ps.executeUpdate();
			if (count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}

	}*/

}