package kai.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import kai.beans.User;
import kai.beans.UserComment;
import kai.exception.NoRowsUpdatedRuntimeException;
import kai.service.MessageService;
import kai.service.UserService;

@WebServlet(urlPatterns = { "/management" })
public class ManagementServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	List<User> users = new UserService().managementUsers();
        List<UserComment> messages = new MessageService().getComment();

        request.setAttribute("users", users);
        request.setAttribute("messages", messages);

        request.getRequestDispatcher("/management.jsp").forward(request, response);
    }

    @Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		User editUser = getEditUser(request);

		try {
			new UserService().stateChange(editUser);
		} catch (NoRowsUpdatedRuntimeException e) {
			messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
			session.setAttribute("errorMessages", messages);
		}

		List<User> users = new UserService().managementUsers();
		request.setAttribute("users", users);
		request.getRequestDispatcher("management.jsp").forward(request, response);
	}

	private User getEditUser(HttpServletRequest request)
			throws IOException, ServletException {

		User editUser = new User();
		editUser.setId(Integer.parseInt(request.getParameter("id")));
		editUser.setAccount(Integer.parseInt(request.getParameter("account")));
		return editUser;
	}
}