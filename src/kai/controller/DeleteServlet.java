package kai.controller;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import kai.beans.Comment;
import kai.beans.Post;
import kai.beans.UserComment;
import kai.beans.UserPost;
import kai.exception.NoRowsUpdatedRuntimeException;
import kai.service.MessageService;

@WebServlet(urlPatterns = { "/delete" })
public class DeleteServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();

		try {
			int postId = Integer.parseInt(request.getParameter("postId"));
			if (postId == -1) {
				Comment deleteComment = getDeleteComment(request);
				new MessageService().commentDelete(deleteComment);
			} else {
				Post deletePost = getDeletePost(request);
				new MessageService().postDelete(deletePost);
			}
		} catch (NoRowsUpdatedRuntimeException e) {
			messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
			session.setAttribute("errorMessages", messages);
		}

		Date dateStart = Date.valueOf("2019-01-01");
		Date dateEnd = Date.valueOf("2020-01-01");
		String category = null;
		List<UserPost> posts = new MessageService().getPost(dateStart, dateEnd, category);
        request.setAttribute("posts", posts);
        List<UserComment> comments = new MessageService().getComment();
        request.setAttribute("comments", comments);
		request.getRequestDispatcher("home.jsp").forward(request, response);
	}

	private Post getDeletePost(HttpServletRequest request)
			throws IOException, ServletException {

		Post deletePost = new Post();
		deletePost.setId(Integer.parseInt(request.getParameter("postId")));
		return deletePost;
	}

	private Comment getDeleteComment(HttpServletRequest request)
			throws IOException, ServletException {

		Comment deleteComment = new Comment();
		deleteComment.setId(Integer.parseInt(request.getParameter("commentId")));
		return deleteComment;
	}

}