package kai.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import kai.beans.Post;
import kai.beans.User;
import kai.beans.UserPost;
import kai.service.MessageService;

@WebServlet(urlPatterns = { "/newPost" })
public class NewPostServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        User user = (User) request.getSession().getAttribute("loginUser");

        request.setAttribute("loginUser", user);
        request.getRequestDispatcher("newPost.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();

        List<String> messages = new ArrayList<String>();

        if (isValid(request, messages) == true) {

            Post post = new Post();
            post.setText(request.getParameter("text"));
            post.setTitle(request.getParameter("title"));
            post.setCategory(request.getParameter("category"));
            post.setIsDeleted(Integer.parseInt(request.getParameter("isDeleted")));
            post.setUserId(Integer.parseInt(request.getParameter("userId")));

            new MessageService().register(post);

            List<UserPost> posts = new MessageService().getPost();

            request.setAttribute("posts", posts);
            request.getRequestDispatcher("home.jsp").forward(request, response);
        } else {
            session.setAttribute("errorMessages", messages);
            response.sendRedirect("newPost");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {

        String text = request.getParameter("text");
        String title = request.getParameter("title");
        String category = request.getParameter("category");

        if (StringUtils.isEmpty(title) == true) {
            messages.add("件名を入力してください");
        }
        if (30 < title.length()) {
            messages.add("件名を30文字以下で入力してください");
        }
        if (StringUtils.isEmpty(text) == true) {
            messages.add("本文を入力してください");
        }
        if (1000 < text.length()) {
            messages.add("本文を1000文字以下で入力してください");
        }
        if (StringUtils.isEmpty(category) == true) {
            messages.add("カテゴリーを入力してください");
        }
        if (10 < category.length()) {
            messages.add("カテゴリーを10文字以下で入力してください");
        }

        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}