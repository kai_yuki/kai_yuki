package kai.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import kai.beans.Branch;
import kai.beans.Position;
import kai.beans.User;
import kai.service.DepartmentService;
import kai.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	 List<Branch> branches = new DepartmentService().branchesList();
         request.setAttribute("branches", branches);
         List<Position> positions = new DepartmentService().positionsList();
         request.setAttribute("positions", positions);

        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<String> messages = new ArrayList<String>();

        HttpSession session = request.getSession();
        if (isValid(request, messages) == true) {

            User user = new User();
            user.setLoginId(request.getParameter("login_id"));
            user.setPassword(request.getParameter("password"));
            user.setName(request.getParameter("name"));
            user.setBranch(Integer.parseInt(request.getParameter("branch")));
            user.setPosition(Integer.parseInt(request.getParameter("position")));
            user.setAccount(Integer.parseInt(request.getParameter("account")));

            new UserService().register(user);

            List<User> users = new UserService().managementUsers();
			request.setAttribute("users", users);
			request.getRequestDispatcher("management.jsp").forward(request, response);
        } else {
            session.setAttribute("errorMessages", messages);
            response.sendRedirect("signup");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {
        String loginId = request.getParameter("login_id");
        String password = request.getParameter("password");
        String checkPassword = request.getParameter("checkPassword");
        String name = request.getParameter("name");
        int branch = Integer.parseInt(request.getParameter("branch"));
        int position = Integer.parseInt(request.getParameter("position"));
        //int account = Integer.parseInt(request.getParameter("account"));

        if (StringUtils.isEmpty(loginId) == true) {
            messages.add("ログインIDを入力してください");
        } else if (loginId.length() < 6 || loginId.length() > 20) {
        	messages.add("ログインIDを6～20文字で入力してください");
        }else {
        	User user = new User();
        	user = UserService.checkUser(loginId);
        	if(user != null) {
        		messages.add("ログインIDを変更してください");
        	}
        }
        if (StringUtils.isEmpty(password) == true) {
            messages.add("パスワードを入力してください");
        } else if (!password.equals(checkPassword)) {
			messages.add("パスワードを正確に入力してください");
		} else if (password.length() < 6 || password.length() > 20) {
			messages.add("パスワードを6～20文字で入力してください");
		}
        if (StringUtils.isEmpty(name) == true) {
            messages.add("名称を入力してください");
        } else if (name.length() > 10) {
			messages.add("名称を10文字以内で入力してください");
		}
        if (branch == 1 && position >= 3) {
            messages.add("支店と役職を確認してください");
        }
        if (branch != 1 && position <= 2) {
			messages.add("支店と役職を確認してください");
		}

        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}