package kai.controller;

import java.io.IOException;
import java.sql.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kai.beans.UserComment;
import kai.beans.UserPost;
import kai.service.MessageService;

@WebServlet(urlPatterns = { "/search" })
public class SearchServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	Date dateStart = Date.valueOf("2019-01-01");
		Date dateEnd = Date.valueOf("2019-12-31");
		String category = null;
    	int search = Integer.parseInt(request.getParameter("search"));
    	if (search == 1) {
    		dateStart = Date.valueOf(request.getParameter("dateStart"));
    		dateEnd = Date.valueOf(request.getParameter("dateEnd"));
    		category = request.getParameter("category");
    	}

        List<UserPost> posts = new MessageService().getPost(dateStart, dateEnd, category);
        List<UserComment> comments = new MessageService().getComment();

        request.setAttribute("posts", posts);
        request.setAttribute("comments", comments);

        request.getRequestDispatcher("/home.jsp").forward(request, response);
    }

}