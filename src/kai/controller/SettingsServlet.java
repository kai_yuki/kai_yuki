package kai.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import kai.beans.Branch;
import kai.beans.Position;
import kai.beans.User;
import kai.exception.NoRowsUpdatedRuntimeException;
import kai.service.DepartmentService;
import kai.service.UserService;

@WebServlet(urlPatterns = { "/settings" })
public class SettingsServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

    	int checker = 0;
    	HttpSession session = request.getSession();
    	if (request.getParameter("id") == null) {
    		checker = 1;
    	} else {
    		User user = (User) session.getAttribute("loginUser");
    		int id = Integer.parseInt(request.getParameter("id"));
    		List<User> users = new UserService().managementUsers();
    		if (id == user.getId() || id > users.size()) {
    			checker = 1;
    		}
    	}
    	if(checker == 1) {
    		List<String> messages = new ArrayList<String>();
    		messages.add("ダメです");
    		session.setAttribute("errorMessages", messages);
    		List<User> users = new UserService().managementUsers();
			request.setAttribute("users", users);
			request.getRequestDispatcher("management.jsp").forward(request, response);
    	} else {
    		User editUser = new UserService().getUser(Integer.parseInt(request.getParameter("id")));
            request.setAttribute("editUser", editUser);

            List<Branch> branches = new DepartmentService().branchesList();
            request.setAttribute("branches", branches);
            List<Position> positions = new DepartmentService().positionsList();
            request.setAttribute("positions", positions);

            request.getRequestDispatcher("settings.jsp").forward(request, response);
    	}
    }

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();
		User editUser = getEditUser(request);

		if (isValid(request, messages) == true) {

			try {
				new UserService().update(editUser);
			} catch (NoRowsUpdatedRuntimeException e) {
				messages.add("他の人によって更新されています。最新のデータを表示しました。データを確認してください。");
				session.setAttribute("errorMessages", messages);
				request.setAttribute("editUser", editUser);
				request.getRequestDispatcher("settings.jsp").forward(request, response);
				return;
			}

			session.setAttribute("editUser", editUser);

			List<User> users = new UserService().managementUsers();
			request.setAttribute("users", users);
			request.getRequestDispatcher("management.jsp").forward(request, response);
		} else {
			session.setAttribute("errorMessages", messages);
			request.setAttribute("editUser", editUser);
			List<Branch> branches = new DepartmentService().branchesList();
	        request.setAttribute("branches", branches);
	        List<Position> positions = new DepartmentService().positionsList();
	        request.setAttribute("positions", positions);
			request.getRequestDispatcher("settings.jsp").forward(request, response);
		}
	}

	private User getEditUser(HttpServletRequest request)
			throws IOException, ServletException {

		User editUser = new User();
		editUser.setId(Integer.parseInt(request.getParameter("id")));
		editUser.setLoginId(request.getParameter("loginId"));
		editUser.setName(request.getParameter("name"));
		editUser.setBranch(Integer.parseInt(request.getParameter("branch")));
		editUser.setPosition(Integer.parseInt(request.getParameter("position")));
		editUser.setAccount(Integer.parseInt(request.getParameter("account")));
		return editUser;
	}


	private boolean isValid(HttpServletRequest request, List<String> messages) {

		int id = Integer.parseInt(request.getParameter("id"));
		String loginId = request.getParameter("loginId");
		String password = request.getParameter("password");
		String checkPassword = request.getParameter("checkPassword");
		String name = request.getParameter("name");
		int branch = Integer.parseInt(request.getParameter("branch"));
		int position = Integer.parseInt(request.getParameter("position"));
		//int account = Integer.parseInt(request.getParameter("account"));

		if (StringUtils.isEmpty(loginId) == true) {
			messages.add("ログインIDを入力してください");
        } else if (loginId.length() < 6 || loginId.length() > 20) {
        	messages.add("ログインIDを6～20文字で入力してください");
        } else {
        	User user = new User();
        	user = UserService.checkUser(loginId);
        	if(user != null && user.getId() != id) {
        		messages.add("ログインIDを変更してください");
        	}
        }
		if (!password.equals(checkPassword)) {
			messages.add("パスワードを正確に入力してください");
		} else if (StringUtils.isEmpty(password) != true && password.length() < 6 || password.length() > 20) {
			messages.add("パスワードを6～20文字で入力してください");
		}
		if (StringUtils.isEmpty(name) == true) {
			messages.add("名称を入力してください");
		} else if (name.length() > 10) {
			messages.add("名称を10文字以内で入力してください");
		}
		if (branch == 1 && position >= 3) {
            messages.add("支店と役職を確認してください");
        }
        if (branch != 1 && position <= 2) {
			messages.add("支店と役職を確認してください");
		}

		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}