package kai.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import kai.beans.Comment;
import kai.beans.UserComment;
import kai.beans.UserPost;
import kai.service.MessageService;

@WebServlet(urlPatterns = { "/home" })
public class HomeServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<UserPost> posts = new MessageService().getPost();
        List<UserComment> comments = new MessageService().getComment();

        request.setAttribute("posts", posts);
        request.setAttribute("comments", comments);

        request.getRequestDispatcher("/home.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();

        List<String> messages = new ArrayList<String>();

        if (isValid(request, messages) == true) {

        	Comment comment = new Comment();
            comment.setText(request.getParameter("comment"));
            comment.setIsDeleted(Integer.parseInt(request.getParameter("isDeleted")));
            comment.setUserId(Integer.parseInt(request.getParameter("userId")));
            comment.setPostId(Integer.parseInt(request.getParameter("postId")));

            new MessageService().register(comment);

            List<UserPost> posts = new MessageService().getPost();
            List<UserComment> comments = new MessageService().getComment();

            request.setAttribute("posts", posts);
            request.setAttribute("comments", comments);
            request.getRequestDispatcher("home.jsp").forward(request, response);
        } else {
            session.setAttribute("errorMessages", messages);
            response.sendRedirect("home");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {

        String comment = request.getParameter("comment");

        if (StringUtils.isEmpty(comment) == true) {
            messages.add("コメントを入力してください");
        }
        if (500 < comment.length()) {
            messages.add("500文字以下で入力してください");
        }
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}