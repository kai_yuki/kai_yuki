package kai.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import kai.beans.User;

@WebFilter("/*")
public class LoginFilter implements Filter {


	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();

		String uri = ((HttpServletRequest)request).getRequestURI();
		HttpSession session = ((HttpServletRequest)request).getSession(false);
		//css経由で死なないために
		if (uri.equals("/kai_yuki/css/style.css")) {
			chain.doFilter(request, response);
			return;
		}
		//ログイン画面の接続
		if (uri.equals("/kai_yuki/") || uri.equals("/kai_yuki/login.jsp")) {
			//ログインしているとき→ダメ→ホームへ
			if (session != null && session.getAttribute("loginUser") != null) {
        		messages.add("ダメです。");
	            session.setAttribute("errorMessages", messages);
	            ((HttpServletResponse)response).sendRedirect("home");
	            return;
			}
			//ログインしていないとき→OK→ログイン画面へ
			chain.doFilter(request, response);
			return;
		}
		//ログイン画面以外
		//ログインしていないとき→ダメ→ログイン画面へ
		if (session == null || session.getAttribute("loginUser") == null){
			messages.add("ログインしてください。");
			session.setAttribute("errorMessages", messages);
			((HttpServletResponse)response).sendRedirect("./");
			return;
		}
		//ログインしているとき
		//権限がいらない存在するページ→OK→各ページへ
		if ( uri.equals("/kai_yuki/search") || uri.equals("/kai_yuki/home") || uri.equals("/kai_yuki/newPost") || uri.equals("/kai_yuki/delete") || uri.equals("/kai_yuki/logout")) {
			chain.doFilter(request, response);
			return;
		}
		//権限がいるページ
		User user = (User) ((HttpServletRequest)request).getSession().getAttribute("loginUser");
		if (uri.equals("/kai_yuki/management") || uri.equals("/kai_yuki/settings") || uri.equals("/kai_yuki/signup")) {
			//本社総務部→OK→各ページへ
			if (user.getPosition() == 1) {
				chain.doFilter(request, response);
				return;
	        }
			//本社総務部以外→ダメ→ホームへ
			messages.add("権限がありません。");
			session.setAttribute("errorMessages", messages);
			((HttpServletResponse)response).sendRedirect("home");
			return;
		}
		//存在しないページ
		messages.add("ダメです。");
		session.setAttribute("errorMessages", messages);
		((HttpServletResponse)response).sendRedirect("home");
		return;
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}

	@Override
	public void destroy() {
	}

}
