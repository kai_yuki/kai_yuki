package kai.service;

import static kai.utils.CloseableUtil.*;
import static kai.utils.DBUtil.*;

import java.sql.Connection;
import java.sql.Date;
import java.util.List;

import kai.beans.Comment;
import kai.beans.Post;
import kai.beans.UserComment;
import kai.beans.UserPost;
import kai.dao.CommentDao;
import kai.dao.PostDao;
import kai.dao.UserCommentDao;
import kai.dao.UserPostDao;


public class MessageService {

	public void register(Post post) {

        Connection connection = null;
        try {
            connection = getConnection();

            PostDao postDao = new PostDao();
            postDao.insert(connection, post);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

	public List<UserPost> getPost() {

    	Connection connection = null;
    	try {
    		connection = getConnection();

    		UserPostDao postDao = new UserPostDao();
    		List<UserPost> ret = postDao.getUserPosts(connection, LIMIT_NUM);

    		commit(connection);

    		return ret;
    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch (Error e) {
    		rollback(connection);
    		throw e;
    	} finally {
    		close(connection);
    	}
    }

    public void register(Comment comment) {

        Connection connection = null;
        try {
            connection = getConnection();

            CommentDao commentDao = new CommentDao();
            commentDao.insert(connection, comment);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    private static final int LIMIT_NUM = 1000;

    public List<UserComment> getComment() {

    	Connection connection = null;
    	try {
    		connection = getConnection();

    		UserCommentDao commentDao = new UserCommentDao();
    		List<UserComment> ret = commentDao.getUserComments(connection, LIMIT_NUM);

    		commit(connection);

    		return ret;
    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch (Error e) {
    		rollback(connection);
    		throw e;
    	} finally {
    		close(connection);
    	}
    }

    public void commentDelete(Comment comment) {

		Connection connection = null;
		try {
			connection = getConnection();

			CommentDao commentDao = new CommentDao();
			commentDao.commentDelete(connection, comment);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
    public void postDelete(Post post) {

		Connection connection = null;
		try {
			connection = getConnection();

			PostDao postDao = new PostDao();
			postDao.postDelete(connection, post);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

    public List<UserPost> getPost(Date dateStart, Date dateEnd, String category) {

    	Connection connection = null;
    	try {
    		connection = getConnection();

    		UserPostDao postDao = new UserPostDao();
    		List<UserPost> ret = postDao.getUserPosts(connection, dateStart, dateEnd, category, LIMIT_NUM);

    		commit(connection);

    		return ret;
    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch (Error e) {
    		rollback(connection);
    		throw e;
    	} finally {
    		close(connection);
    	}
    }

}