package kai.service;

import static kai.utils.CloseableUtil.*;
import static kai.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import kai.beans.Branch;
import kai.beans.Position;
import kai.dao.BranchDao;
import kai.dao.PositionDao;

public class DepartmentService {

	/*
	//支店が増えたら使ってください。
    public void register(Branch branch) {

        Connection connection = null;
        try {
            connection = getConnection();

            BranchDao branchDao = new BranchDao();
            branchDao.insert(connection, branch);

            commit(connection);

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //これが使われる日はおそらくこない
    public Branch getBranch(int branchId) {

    	Connection connection = null;
    	try {
    		connection = getConnection();

    		BranchDao branchDao = new BranchDao();
    		Branch branch = branchDao.getBranch(connection, branchId);

    		commit(connection);

    		return branch;
    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch (Error e) {
    		rollback(connection);
    		throw e;
    	} finally {
    		close(connection);
    	}
    }

    //支店名が変わったら使ってください。
    public void update(Branch branch) {

		Connection connection = null;
		try {
			connection = getConnection();

			BranchDao branchDao = new BranchDao();
			branchDao.update(connection, branch);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}*/

    public List<Branch> branchesList() {

		Connection connection = null;
		try {
			connection = getConnection();

			BranchDao branchDao = new BranchDao();
			List<Branch> branches = branchDao.branchesList(connection);

			return branches;
    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch (Error e) {
    		rollback(connection);
    		throw e;
    	} finally {
    		close(connection);
		}
	}
    public List<Position> positionsList() {

		Connection connection = null;
		try {
			connection = getConnection();

			PositionDao positionDao = new PositionDao();
			List<Position> positions = positionDao.positionsList(connection);

			return positions;
    	} catch (RuntimeException e) {
    		rollback(connection);
    		throw e;
    	} catch (Error e) {
    		rollback(connection);
    		throw e;
    	} finally {
    		close(connection);
		}
	}

}