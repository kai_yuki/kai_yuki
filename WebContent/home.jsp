<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ホーム</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div class="main-contents">
			<div class="header">
				<a href="logout">ログアウト</a>
				<a href="newPost">新規投稿</a>
				<a href="management">ユーザー管理</a>
			</div>
			<br />
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session"/>
			</c:if>
			<br />

			<div class="profile">
				<div class="name"><h2><c:out value="${loginUser.name}" /></h2></div>
			</div>
			<br />

			<form action="search" method="get">
			<label for="date">投稿日(期間指定の時刻は0:00:00なのでお気をつけて)</label><br />

			<input id="dateStart" name="dateStart" type="date" autocomplete="on" value="2019-01-01" required/>から
			<br />
			<input id="dateEnd" name="dateEnd" type="date" autocomplete="on" value="2020-01-01" required//>まで
			<br />
			<label for="category">カテゴリー(部分一致)</label>
			<input id="category" name="category" />
			<br />
			<button type="submit" name="search" value=1>検索</button>
			<button type="submit" name="search" value=0>リセット</button>
			</form>

			<div class="messages">
			    <c:forEach items="${posts}" var="post">
			    <c:if test="${post.isDeleted == 1}">
					<div class="message">
						<div class="account-name">
							<span class="title"><c:out value="${post.title}" /></span>
						</div>
						<div class="text"><c:out value="${post.text}" /></div>
						<div class="category"><c:out value="${post.category}" /></div>
						<div class="date"><fmt:formatDate value="${post.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
						<span class="name"><c:out value="${post.name}" /></span>
						<c:if test="${post.userId == loginUser.id}">
							<form action="delete" method="post" >
							<input  name="postId" value="${post.id}" id="postId" type="hidden"/>
							<input  name="commentId" value=-1 id="commentId" type="hidden"/>
							<button type="submit" name="isDeleted" value=0 id="isDeleted" onClick="return checkDialog()">削除</button>
							<script>
							function checkDialog(){
								var result=confirm("投稿を削除します？");
								return result;
							}
							</script>
							</form>
						</c:if>
					</div>
					<c:forEach items="${comments}" var="comment">
						<c:if test="${comment.isDeleted == 1 and comment.postId == post.id}">
							<div class="message">
								<div class="text"><c:out value="${comment.text}" /></div>
								<div class="date"><fmt:formatDate value="${comment.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
								<div class="name"><c:out value="${comment.name}" /></div>
									<c:if test="${comment.userId == loginUser.id}">
									<form action="delete" method="post" >
									<input  name="postId" value=-1 id="postId" type="hidden"/>
									<input  name="commentId" value="${comment.id}" id="commentId" type="hidden"/>
									<button type="submit" name="isDeleted" value=0 id="isDeleted" onClick="return checkDialog()">削除</button>
									<script>
									function checkDialog(){
										var result=confirm("コメントを削除します？");
										return result;
									}
									</script>
									</form>
									</c:if>
							</div>
						</c:if>
					</c:forEach>
					<form action="home" method="post">
					<input  name="postId" value="${post.id}" id="postId" type="hidden"/>
					<label for="comment">コメント</label>
					<textarea name="comment" cols="100" rows="5" class="tweet-box" maxlength="500" required></textarea><br />
					<input name="userId" value="${loginUser.id}" type="hidden"/>
					<input name="isDeleted" value=1 type="hidden"/>
					<input type="submit" value="コメントする">（500文字まで）
					</form>
					</c:if>
					</c:forEach>
					<br /><br />
				</div>
			<div class="copylight"> Copyright(c)2112</div>
		</div>
	</body>
</html>