<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ユーザー管理</title>
		<link href="./css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div class="main-contents">
			<div class="header">
				<a href="logout">ログアウト</a>
				<a href="signup">新規登録</a>
				<a href="home">ホーム</a>
			</div>
			<br />

			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session"/>
			</c:if>

			<div class="profile">
				<div class="name"><h2><c:out value="${loginUser.name}" /></h2></div>
			</div>

			<div class="list">
				<table>
				<caption>ユーザーの一覧</caption>
					<tr>
						<th>編集画面へ</th>
						<th>ID</th>
						<th>ログインID</th>
						<th>名称</th>
						<th>支店</th>
						<th>部署・役職</th>
						<th>状態</th>
					</tr>
					<c:forEach items="${users}" var="user">
						<tr>
						<td>
						<c:if test="${user.id == loginUser.id}">ログイン中</c:if>
						<c:if test="${user.id != loginUser.id}">
						<form action="settings" method="get">
						<button type="submit" name="id" value="${user.id}" id="id">編集</button>
						</form>
						</c:if>
						</td>
						<td>${user.id}</td>
						<td>${user.loginId}</td>
						<td>${user.name}</td>
						<td>${user.branchName}</td>
						<td>${user.positionName}</td>
						<td>
						<c:if test="${user.id == loginUser.id}">ログイン中</c:if>
						<c:if test="${user.id != loginUser.id}">
						<form action="management" method="post">
						<input  name="id" value="${user.id}" id="id" type="hidden"/>
						<c:if test="${user.account == 1}">
						<button type="submit" name="account" value=0 id="account" onClick="return checkDialog()">
						<span onmouseover="this.innerText='停止'" onmouseout="this.innerText='稼働中'">稼働中
						</span></button></c:if>
						<c:if test="${user.account == 0}">
						<button type="submit" name="account" value=1 id="account" onClick="return checkDialog()">
						<span onmouseover="this.innerText='稼働'" onmouseout="this.innerText='停止中'">停止中
						</span></button></c:if>
						<script>
						function checkDialog(){
							var result=confirm("変更しますよ？");
							return result;
						}
						</script>
						</form>
						</c:if>
						</td>
						</tr>
					</c:forEach>
				</table>
			</div>

			<div class="copylight"> Copyright(c)2112</div>
		</div>
	</body>
</html>