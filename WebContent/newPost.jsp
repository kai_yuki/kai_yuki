<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>新規投稿</title>
		<link href="css/style.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div class="main-contents">
			<div class="header">
				<a href="logout">ログアウト</a>
				<a href="home">ホーム</a>
			</div>
			<br />
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session"/>
			</c:if>
			<br />

			<c:if test="${ not empty loginUser }">
			    <div class="profile">
			        <div class="name"><h2><c:out value="${loginUser.name}" /></h2></div>
			    </div>
			</c:if>

			<div class="form-area">
				<form action="newPost" method="post"><br />
					<label for="title">件名</label>
					<input name="title" id="title" pattern="^.{1,30}$" required/> <br />
					<label for="text">本文</label>
					<textarea name="text" cols="50" rows="20" class="tweet-box" maxlength="1000" required></textarea>
					<br />
					<label for="category">カテゴリー</label>
					<input name="category" id="category" pattern="^.{1,10}$" required/> <br />
					<input name="userId" value="${loginUser.id}" type="hidden"/> <br />
					<input name="isDeleted" value=1 type="hidden"/> <br />
					<input type="submit" value="投稿する">（1000文字まで）
				</form>
			</div>
			<div class="copylight"> Copyright(c)2112</div>
		</div>
	</body>
</html>