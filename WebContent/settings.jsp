<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>${editUser.name}の設定</title>
        <link href="css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="main-contents">
        	<div class="header">
				<a href="logout">ログアウト</a>
				<a href="home">ホーム</a>
				<a href="management">ユーザー管理</a>
			</div>
			<br />

            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session"/>
            </c:if>

            <form action="settings" method="post" ><br />
            	<input name="id" value="${editUser.id}" id="id" type="hidden"/>

            	<label for="loginId">ログインID(半角英数字[azAZ0*9]で6文字以上20文字以下)</label>
                <input name="loginId" value="${editUser.loginId}" id="loginId" pattern="^[a-zA-Z0-9]{6,20}$" required/>

                <label for="password">パスワード(記号を含む全ての半角文字で6文字以上20文字以下)</label>
                <input name="password" type="password" id="password" pattern="^[ -~]{6,20}$"/> <br />

				<label for="checkPassword">パスワード確認用</label>
                <input name="checkPassword" type="password" id="checkPassword" pattern="^[ -~]{6,20}$"/> <br />

                <label for="name">名称(10文字以下)</label>
                <input name="name" value="${editUser.name}" id="name" pattern="^.{1,10}$" required/><br />

                <label for="branch">支店名</label>
                <select id="branch" name="branch">
                <c:forEach items="${branches}" var="branch">
                <option value="${branch.id}">${branch.name}</option>
                </c:forEach>
                <script>
                document.getElementById('branch').options["${editUser.branch}"-1].selected = true;
                </script>
                </select>
                <br />

                <label for="position">部署・役職</label>
                <select id="position" name="position">
                <c:forEach items="${positions}" var="position">
                <option value="${position.id}">${position.name}</option>
                </c:forEach>
                <script>
                document.getElementById('position').options["${editUser.position}"-1].selected = true;
                </script>
                </select>
                <br />

                <input name="account" value="${editUser.account}" id="account" type="hidden"/>
                <br />

                <input type="submit" value="変更" /> <br />
                <a href="management">戻る</a>
            </form>
            <div class="copyright"> Copyright(c)2112</div>
        </div>
    </body>
</html>